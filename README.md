# README #

This README mention the logic of cab booking application.

### How do I run the application? ###

* Setup the project using Intellij IDE, run `mvn clean install` and make sure build is success without any testcase failures
* Project Configuration can be found in properties files under resources folder (`/src/main/resources`)
* Related jars/dependencies can be found in `pom.xml` files
* configuration for each environment will be present in following files
    * Local - `application-default.properties`  
    * Preprod - `application-preprod.properties`
    * Production - `application-production.properties`


### Implementation Logic? ###

* MongoDB has been used as a database and configuration details can be found in application.properties file
* Entity classes : Persona, Rider, Driver, Vehicle, Booking, City

* Controller classes are 
     * BookingController - It has functions as : initiateBooking, confirmBooking, getBookingDetails, updateBooking, endTrip.
     * VehicleController - It has functions as : registerVehicle, updateLocation, update, findByLatLon, findByVehicleNumber
     * RiderController - It has functions as : registerRider, verifyRider
     * DriverController -  It has functions as : registerDriver, verifyDriver 
     * CityController -  It has functions as : onboardCity
     * HealthCheck

* Service classes are 
     * BookingService - It has functions as : initiateBooking, confirmBooking, getBookingDetails, updateBooking, endTrip.
     * VehicleService - It has functions as : registerVehicle, updateLocation, update, findByLatLon, findByVehicleNumber
     * RiderService - It has functions as : registerRider
     * DriverService -  It has functions as : registerDriver
     * CityService -  It has functions as : onBoardCity
  
    
### Check before running application? ###
* Before running the application, make sure that mongoDb is running locally on your system - mongodb://localhost:27017/cab

### Assumption? ###
* It's difficult to calculate exact distance from lat/long using simple math.sqrt function. Current assumption is that those vehicles are fetch which
  exactly match lat and long for source .
* As of now, distance is being kept in booking entity class and assumed that some external is providing it









    

