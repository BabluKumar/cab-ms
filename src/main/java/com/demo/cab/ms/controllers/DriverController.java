package com.demo.cab.ms.controllers;

import com.demo.cab.ms.entity.Driver;
import com.demo.cab.ms.service.IDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/driver")
public class DriverController {

  @Autowired
  private IDriverService driverService;

  @RequestMapping(value = "/v1", method = RequestMethod.POST, produces = "application/json")
  public Driver register(@RequestBody(required = true) Driver driver) {

    return driverService.register(driver);
  }



}
