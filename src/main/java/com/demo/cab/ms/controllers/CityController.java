package com.demo.cab.ms.controllers;

import com.demo.cab.ms.entity.City;
import com.demo.cab.ms.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private ICityService cityService;

    @RequestMapping(value = "/v1", method = RequestMethod.POST, produces = "application/json")
    public City onboardCity(@RequestBody(required = true) City city) {
        return cityService.onBoardCity(city);
    }

}
