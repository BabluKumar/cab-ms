package com.demo.cab.ms.controllers;

import com.demo.cab.ms.entity.Rider;
import com.demo.cab.ms.service.IRiderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rider")
public class RiderController {

  @Autowired
  private IRiderService riderService;

  @RequestMapping(value = "/v1", method = RequestMethod.POST, produces = "application/json")
  public Rider register(@RequestBody(required = true) Rider rider) {
    return riderService.register(rider);
  }


}
