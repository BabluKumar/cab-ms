package com.demo.cab.ms.repositories;

import com.demo.cab.ms.entity.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends MongoRepository<Vehicle, String> {

  List<Vehicle> findByLatAndLonAndIsAvailable(Double lat, Double lon, Boolean isAvailable);
  Vehicle findByVehicleNumber(String vehicleNumber);

}
