package com.demo.cab.ms.entity.enums;

public enum VehicleStatus {
    IDLE,
    ON_TRIP
}
