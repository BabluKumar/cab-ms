package com.demo.cab.ms.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@Builder
@Document(collection = "city")
public class City implements Serializable {

    @Id
    private String cityId;
    private String cityName;

}
