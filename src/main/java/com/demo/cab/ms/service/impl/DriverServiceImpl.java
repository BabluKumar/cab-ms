package com.demo.cab.ms.service.impl;

import com.demo.cab.ms.entity.Driver;
import com.demo.cab.ms.entity.enums.PersonaType;
import com.demo.cab.ms.exceptions.BadRequest;
import com.demo.cab.ms.repositories.DriverRepository;
import com.demo.cab.ms.service.IDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class DriverServiceImpl implements IDriverService {

  private static final Logger LOGGER = LoggerFactory.getLogger(DriverServiceImpl.class);

  @Autowired
  private DriverRepository driverRepository;

  @Override
  public Driver register(Driver driver) {
    if(ObjectUtils.isEmpty(driver) || ObjectUtils.isEmpty(driver.getPhoneNumber())) {
      throw new BadRequest("invalid driver registration request body");
    }
    LOGGER.info("registering driver with phone number {} ",driver.getPhoneNumber());
    driver.setPersonaType(PersonaType.Driver);
    return driverRepository.save(driver);
  }


}
