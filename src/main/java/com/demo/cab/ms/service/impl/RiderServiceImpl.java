package com.demo.cab.ms.service.impl;

import com.demo.cab.ms.entity.Rider;
import com.demo.cab.ms.entity.enums.PersonaType;
import com.demo.cab.ms.exceptions.BadRequest;
import com.demo.cab.ms.repositories.RiderRepository;
import com.demo.cab.ms.service.IRiderService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class RiderServiceImpl implements IRiderService {


  private static final Logger LOGGER = LoggerFactory.getLogger(RiderServiceImpl.class);

  @Autowired
  private RiderRepository riderRepository;


  @Override
  public Rider register(Rider rider) {
    if(ObjectUtils.isEmpty(rider) || ObjectUtils.isEmpty(rider.getPhoneNumber())) {
      throw new BadRequest("invalid rider registration request body");
    }
    LOGGER.info("registering rider with phone number {} ",rider.getPhoneNumber());
    rider.setPersonaType(PersonaType.Rider);
    return riderRepository.save(rider);
  }


}
