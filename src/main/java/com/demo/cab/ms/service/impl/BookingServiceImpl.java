package com.demo.cab.ms.service.impl;

import com.demo.cab.ms.entity.Booking;
import com.demo.cab.ms.entity.Vehicle;
import com.demo.cab.ms.entity.enums.BookingStatus;
import com.demo.cab.ms.entity.enums.VehicleStatus;
import com.demo.cab.ms.exceptions.BadRequest;
import com.demo.cab.ms.repositories.BookingRepository;
import com.demo.cab.ms.service.IBookingService;
import com.demo.cab.ms.service.IVehicleService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class BookingServiceImpl implements IBookingService {


  private static final Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);

  @Autowired
  private BookingRepository bookingRepository;

  @Autowired
  private IVehicleService vehicleService;

  private static final double BOOKING_FEE = 20.0;

  @Override
  public Booking initiateBooking(String riderUserId, Double srclat, Double srclon, Double destlat, Double destlon, Double distance) {
    if(ObjectUtils.isEmpty(riderUserId) || ObjectUtils.isEmpty(srclat) || ObjectUtils.isEmpty(srclon)){
      throw new BadRequest("invalid booking request. Please check riderId, lat, long");
    }
    LOGGER.info("initiating booking for rider {}", riderUserId);
    Booking booking = Booking.builder().riderUserId(riderUserId)
        .initiatedTime(System.currentTimeMillis()).sourceLat(srclat).sourceLon(srclon).distance(distance).
            destinationLat(destlat).destinationLon(destlon).
            bookingStatus(BookingStatus.INITIATED).build();
    return bookingRepository.save(booking);
  }

  @Override
  public Booking confirmBooking(String bookingId, String driverId) {
    if(ObjectUtils.isEmpty(bookingId) || ObjectUtils.isEmpty(driverId)){
      throw new BadRequest("invalid booking confirmation request. Please check bookingId, driverId");
    }
    LOGGER.info("confirming booking {}", bookingId);
    Optional<Booking> booking = bookingRepository.findById(bookingId);
    if(!booking.isPresent()){
      throw new BadRequest("booking not found");
    }
    Booking currentBooking = booking.get();
    Vehicle vehicle = findOptimalVehicle(currentBooking.getSourceLat(), currentBooking.getSourceLon());
    currentBooking.setVehicleNumber(vehicle.getVehicleNumber());
    currentBooking.setDriverUserId(vehicle.getDriverId());
    currentBooking.setBookingStatus(BookingStatus.CONFIRMED);
    currentBooking.setStartTime(System.currentTimeMillis());
    vehicle.setAvailable(false);
    vehicle.setVehicleStatus(VehicleStatus.ON_TRIP);
    return bookingRepository.save(currentBooking);
  }

  private Vehicle findOptimalVehicle(Double sourceLat,Double sourceLon){
    List<Vehicle> vehicleList = vehicleService.find(sourceLat,sourceLon);
    Map<String,Vehicle> vehicleNumToVechicleMap = new HashMap<>();
    String selectedVehicleNumber = "";
    long minBusy = Long.MAX_VALUE;
    for(Vehicle v : vehicleList){
      vehicleNumToVechicleMap.put(v.getVehicleNumber(),v);
      List<Booking> bookings = getBookingByVehicleNumber(v.getVehicleNumber());
      if(ObjectUtils.isEmpty(bookings)){
        /*
        vehicle with no bookings till data.
         */
        return v;
      }
      long totalRideDuration = 0;
      for(Booking booking : bookings){
        totalRideDuration = totalRideDuration +  booking.getRideDuration();
      }
      if(minBusy > totalRideDuration){
        minBusy = totalRideDuration;
        selectedVehicleNumber = v.getVehicleNumber();
      }
    }
    return vehicleNumToVechicleMap.get(selectedVehicleNumber);
  }

  @Override
  public Booking getBookingDetails(String bookingId) {
    LOGGER.info("fetching booking details for bookingId {}", bookingId);
    Optional<Booking> booking = bookingRepository.findById(bookingId);
    if(!booking.isPresent()){
      throw new BadRequest("booking not found");
    }
    Booking bookingFromDb = booking.get();
    if(!BookingStatus.CONFIRMED.equals(bookingFromDb.getBookingStatus())){
      throw new BadRequest("booking not confirmed");
    }
    return bookingFromDb;
  }

  @Override
  public Booking updateBooking(Booking booking) {
    if(ObjectUtils.isEmpty(booking) || ObjectUtils.isEmpty(booking.getId())){
      throw new BadRequest("invalid booking update request. Please check bookingId");
    }
    LOGGER.info("updating booking {}", booking.getId());
    Optional<Booking> currentBooking = bookingRepository.findById(booking.getId());
    if(!currentBooking.isPresent()){
      throw new BadRequest("booking not found");
    }
    Booking bookingFromDb = currentBooking.get();
    if(!ObjectUtils.isEmpty(booking.getDestinationLat()))
      bookingFromDb.setDestinationLat(booking.getDestinationLat());
    if(!ObjectUtils.isEmpty(booking.getDestinationLat()))
      bookingFromDb.setDestinationLon(booking.getDestinationLon());
    return bookingRepository.save(bookingFromDb);
  }

  @Override
  public Boolean endTrip(Long timeStamp, String bookingId) {
    if(ObjectUtils.isEmpty(bookingId)){
      throw new BadRequest("invalid bookingId.");
    }
    Optional<Booking> currentBooking = bookingRepository.findById(bookingId);
    if(!currentBooking.isPresent()){
      throw new BadRequest("booking not found");
    }
    Booking bookingFromDb = currentBooking.get();
    bookingFromDb.setEndTime(timeStamp);
    bookingFromDb.setBookingStatus(BookingStatus.ENDED);
    if(!ObjectUtils.isEmpty(bookingFromDb.getEndTime()) && !ObjectUtils.isEmpty(bookingFromDb.getStartTime()))
      bookingFromDb.setRideDuration(bookingFromDb.getEndTime() - bookingFromDb.getStartTime());
    String vehicleNumberInThisBooking = bookingFromDb.getVehicleNumber();
    Vehicle vehicleInThisBooking = vehicleService.findByVehicleNumber(vehicleNumberInThisBooking);
    vehicleInThisBooking.setAvailable(true);
    vehicleInThisBooking.setVehicleStatus(VehicleStatus.IDLE);
    vehicleService.update(vehicleInThisBooking);
    bookingRepository.save(bookingFromDb);
    return true;
  }

  @Override
  public List<Booking> getBookingByVehicleNumber(String vehicleNumber) {
    if(ObjectUtils.isEmpty(vehicleNumber)){
      throw new BadRequest("invalid vehicle number");
    }
    return bookingRepository.findByVehicleNumber(vehicleNumber);
  }


}
