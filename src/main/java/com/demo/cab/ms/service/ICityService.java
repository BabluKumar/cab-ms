package com.demo.cab.ms.service;

import com.demo.cab.ms.entity.City;

public interface ICityService {

    City onBoardCity(City city);

}
