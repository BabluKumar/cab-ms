package com.demo.cab.ms.service.impl;

import com.demo.cab.ms.entity.City;
import com.demo.cab.ms.exceptions.BadRequest;
import com.demo.cab.ms.repositories.CityRepository;
import com.demo.cab.ms.service.ICityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class CityServiceImpl implements ICityService {


    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleServiceImpl.class);

    @Autowired
    private CityRepository cityRepository;

    @Override
    public City onBoardCity(City city) {
        if(ObjectUtils.isEmpty(city) || ObjectUtils.isEmpty(city.getCityName())){
            throw new BadRequest("invalid vehicle registration request body");
        }
        LOGGER.info("onboarding city  {}", city.getCityName());
        city = cityRepository.save(city);
        return city;
    }

}
