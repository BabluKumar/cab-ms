package com.demo.cab.ms.service;

import com.demo.cab.ms.entity.Vehicle;

import java.util.List;

public interface IVehicleService {

  Vehicle registerVehicle(Vehicle vehicle);

  Vehicle update(Vehicle vehicle);

  Vehicle updateLocation(Vehicle vehicle);

  List<Vehicle> find(Double lat, Double lon);

  List<Vehicle> findAll();

  Vehicle findByVehicleNumber(String vehicleNumber);

}
